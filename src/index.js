import Phaser from 'phaser';
import InvisibleHouse from './house';

const config = {
  type: Phaser.AUTO, // Which renderer to use
  width: 800, // Canvas width in pixels
  height: 600, // Canvas height in pixels
  parent: "game-container", // ID of the DOM element to add the canvas to
  scene: [ InvisibleHouse ],
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 0 } // Top down game, so no gravity
    }
  }
};

const game = new Phaser.Game(config); /* eslint-disable-line no-unused-vars */
global.game = game;

// This is needed for Hot Module Replacement
// if (module.hot) { module.hot.accept(); }
