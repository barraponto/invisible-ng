import Phaser from 'phaser';
import EasyStar from 'easystarjs'
import _ from 'underscore';

global._ = _;
export default class InvisibleHouse extends Phaser.Scene {

  constructor() {
    super({ key: 'invisible-house' })
  }

  preload() {
    this.load.spritesheet("tileset1", "static/images/tileset.png", { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet("brinquedos", "static/images/brinquedos.png", { frameWidth: 32, frameHeight: 32 });
    this.load.image("hero", "static/images/hero.png");
    this.load.image("crianca", "static/images/hero.png");
    this.load.image("chinelo", "static/images/chinelo.png");
    this.load.tilemapTiledJSON("map", "static/levels/map.json");
    // Runs once, loads up assets like images and audio
  }
  create() {
    // Runs once, after all assets in preload are loaded
    // Load a map from a 2D array of tile indices

    // When loading from an array, make sure to specify the tileWidth and tileHeight
    this.map = this.make.tilemap({ key: "map", tileWidth: 16, tileHeight: 16 });
    const tiles = this.map.addTilesetImage("house", "tileset1");
    this.layers = {
      under: this.map.createStaticLayer("under", tiles, 0, 0),
      world: this.map.createStaticLayer("world", tiles, 0, 0),
    };

    this.layers.world.setCollisionByProperty({ collision: true });



    const grid = [];
      for(let y = 0; y < this.map.height; y++){
          const col = [];
          for(let x = 0; x < this.map.width; x++){
              // In each cell we store the ID of the tile, which corresponds
              // to its index in the tileset of the map ("ID" field in Tiled)
              const tile = this.map.getTileAt(x,y,-1,'world')
              col.push(tile.index);
          }
          grid.push(col);
      }

    this.finder = new EasyStar.js();

    this.finder.setGrid(grid);
    this.finder.setAcceptableTiles([-1,0,1]); //hackish


    this.crianca = this.physics.add.sprite(350,350, "crianca");
    this.physics.add.collider(this.crianca, this.layers.world);
    this.crianca.tedio = 0;


    this.tedio = this.add.text(10,10, "Tédio: 0%")
    this.tedio.scrollFactorX = 0;
    this.tedio.scrollFactorY = 0;

    this.player = this.physics.add.sprite(641, 503, "hero")
    this.physics.add.collider(this.player, this.layers.world);
    this.physics.add.collider(this.player,this.crianca, () => {
      this.crianca.tedio = 0;
      this.crianca.body.setVelocity(0);
      this.crianca.frozen = true;
      this.time.delayedCall(1000, () => { this.crianca.frozen = false; console.log("rolou"); } );
    });

    const camera = this.cameras.main;
    camera.startFollow(this.player);
    camera.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels);

    this.cursors = this.input.keyboard.createCursorKeys();
  }

  update() {
    const speed = 175;
    const criancaSpeed = 175;
    // Stop any previous movement from the last frame
    this.player.body.setVelocity(0);

    // Horizontal movement
    if (this.cursors.left.isDown) {
      this.player.body.setVelocityX(-speed);
    } else if (this.cursors.right.isDown) {
      this.player.body.setVelocityX(speed);
    }

    // Vertical movement
    if (this.cursors.up.isDown) {
      this.player.body.setVelocityY(-speed);
    } else if (this.cursors.down.isDown) {
      this.player.body.setVelocityY(speed);
    }

    // Normalize and scale the velocity so that player can't move faster along a diagonal
    this.player.body.velocity.normalize().scale(speed);


    // loop da crianca
    if (this.crianca.tedio > 100) {
      const { x,y } = this.crianca.getCenter();
      this.addItem(x, y, "brinquedos");
      this.crianca.tedio = 0;
    }


    if (!this.crianca.frozen) {

     const { x,y } = this.crianca.getCenter();

     const fromX = Math.floor(x/32);
     const fromY = Math.floor(y/32);

     const choices = this.map.filterObjects("travel", ( obj ) => { if (obj.type === 'movepoints') return true; })
     const valid = Phaser.Utils.Array.GetRandom(choices)

     const toX = Math.floor(valid.x/32)
     const toY = Math.floor(valid.y/32)

     this.finder.findPath(fromX, fromY, toX, toY, ( path ) => {
       if (path === null) {
            console.warn("Path was not found.");
         } else {
           this.crianca.frozen = true;
            this.moveCharacter(path);
         }
     });
     this.finder.calculate()
    }
  }
  addItem(x, y, name) {
    const item = this.physics.add.sprite(x,y,name, _.random(6));
    item.body.imovable = true;
    item.body.moves = false;
    this.physics.add.collider(this.player, item, this.pickup.bind(this));
  }

  pickup(player, item) {
    item.destroy();
  }

  moveCharacter(path) {
    // Sets up a list of tweens, one for each tile to walk, that will be chained by the timeline
    var tweens = [];
    for(var i = 0; i < path.length-1; i++){
        var ex = path[i+1].x;
        var ey = path[i+1].y;
        tweens.push({
            targets: this.crianca,
            x: {value: ex*this.map.tileWidth, duration: 200},
            y: {value: ey*this.map.tileHeight, duration: 200}
        });
    }
    console.log(tweens);
    this.tweens.timeline({
        tweens: tweens
    });
  }

}
