module.exports = {
  use: [
    '@neutrinojs/airbnb-base',
    'neutrino-preset-prettier',
    [
      '@neutrinojs/web',
      {
        html: {
          title: 'invisible-ng'
        }
      }
    ],
    '@neutrinojs/jest'
  ]
};
